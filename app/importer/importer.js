import csvtojson from 'csvtojson';

export class Importer {
    constructor(params) {
        this.emitter = params.emitter;
    }

    startImport(path, sync) {
        const importMethod = sync ? this.importSync.bind(this) : this.import.bind(this);
        this.importListener = (change) => {
            importMethod(`${path}/${change.fileName}`)
        };
        this.emitter.on('dirwatcher:changed', this.importListener);
    }

    stopImport() {
        this.emitter.removeListener('dirwatcher:changed', this.importListener);
    }

    convert(filePath, callback) {
        csvtojson()
            .fromFile(filePath)
            .on('json',(jsonObj) => {
                this.emitter.emit('importer:dataConvert', {convertedData:jsonObj});
                return callback ? callback(jsonObj) : jsonObj;
            })
    }


    importSync(path) {
        this.convert(path);
    }

    import(path) {
        return new Promise((resolve, reject) => {
            try {
                this.convert(path, (json) => resolve(json));
            } catch(error) {
                this.emitter.emit('error', error);
                reject(error);
            }
        });
    }

}
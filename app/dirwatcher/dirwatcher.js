import fs from 'fs';
import {CHANGE_TYPES} from './constants';


export class DirWatcher {
    constructor(params) {
        this.fileList = Object.create(null);
        this.emitter = params.emitter;
    }


    changeFileContent (path, fileName) {
        fs.readFile(`${path}/${fileName}`, 'utf-8', (error, data) => {
            if (error) {
                this.emitter.emit('error', error);
                return;
            }
            this.fileList[fileName].content = data;
        });
    };
    addFileToList(path, fileName) {
        this.fileList[fileName] = {};
        this.changeFileContent(path, fileName);
    };
    removeFileFromList(fileName) {
        delete this.fileList[fileName];
    };

    checkFiles(path) {
        fs.readdir(path, (error, nextFiles = []) => {
            if (error) {
                this.emitter.emit('error', error);
                return;
            }
            nextFiles.forEach((fileName) => {
                const filePath = `${path}/${fileName}`;

                if (this.fileList[fileName]) {
                    fs.readFile(filePath, 'utf-8', (error, data) => {
                        if (error) {
                            this.emitter.emit('error', error);
                            return;
                        }
                        const fileContent = this.fileList[fileName].content;
                        if (fileContent !== data) {
                            this.changeFileContent(path, fileName);
                            this.emitter.emit('dirwatcher:changed', {fileName, changeType: CHANGE_TYPES.MODIFY});
                        }
                    });
                } else {
                    this.addFileToList(path, fileName);
                    this.emitter.emit('dirwatcher:changed', {fileName, changeType: CHANGE_TYPES.NEW})
                }
            });
            Object.keys(this.fileList).forEach((fileName) => {
                if (!nextFiles.includes(fileName)) {
                    this.removeFileFromList(fileName);
                    this.emitter.emit('dirwatcher:changed', {fileName, changeType: CHANGE_TYPES.REMOVE})
                }
            });
        });
    }

    watch(path, delay) {
        console.log('Start watching.');
        this.checkFiles(path);

        this.watcherTimer = setInterval(() => {
            this.checkFiles(path);
        }, delay);
    }

    stopWatch() {
        clearInterval(this.watcherTimer);
    }
};
export const CHANGE_TYPES = {
    MODIFY: 'modify',
    NEW: 'new file',
    REMOVE: 'remove'
};
import {EventEmitter} from 'events';
import {DirWatcher} from "./app/dirwatcher/dirwatcher";
import {Importer} from "./app/importer";

const emitter = new EventEmitter();

const watcher = new DirWatcher({emitter});
const importer = new Importer({emitter});

watcher.watch('./app/data', 500);
importer.startImport('./app/data', true);

emitter.on('dirwatcher:changed', (change) => {
    console.log(`File ${change.fileName} was changed. Type of change: ${change.changeType}`);
});

emitter.on('importer:dataConvert', (data) => {
    console.log(data.convertedData)
});

emitter.on('error', (error) => {
    console.error('Error on application:', error);
});
